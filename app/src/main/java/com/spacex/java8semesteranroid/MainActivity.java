package com.spacex.java8semesteranroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity{
    private ViewPagerFragmentAdapter viewPagerFragmentAdapter;
    private TabLayout tabLayout;
    private ViewPager2 viewPager2;
    private String[] titles=new String[]{"Login","Register"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);//set the view on this layout
        findByViews();

    }
    private void findByViews(){
        tabLayout=findViewById(R.id.tab_layout);
        viewPager2=findViewById(R.id.view_pager_id);
        viewPagerFragmentAdapter=new ViewPagerFragmentAdapter(this);
        viewPager2.setAdapter(viewPagerFragmentAdapter);
//        tablayoutmediator sync the tablout and viewpager2 position
        new TabLayoutMediator(tabLayout,viewPager2,((tab, position) -> tab.setText(titles[position]))).attach();
    }
}