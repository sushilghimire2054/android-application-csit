package com.spacex.java8semesteranroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText fname,lname,phnumber,password,confpassword,address;
    private AppCompatButton appCompatButton;

    //defining AwesomeValidation object
    AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);

        //assign variables
        fname=findViewById(R.id.edit_fname);
        lname=findViewById(R.id.edit_lname);
        phnumber=findViewById(R.id.edit_phonenumber);
        password=findViewById(R.id.edit_password);
        confpassword=findViewById(R.id.edit_confpassword);
        address=findViewById(R.id.edit_address);
        appCompatButton=findViewById(R.id.signup_btn);
        //initializing awesomevalidation object
        /*
         * The library provides 3 types of validation
         * BASIC
         * COLORATION
         * UNDERLABEL
         * */
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        //adding validation to edittext
        awesomeValidation.addValidation(this,R.id.edit_fname,"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$",R.string.firstname_error);
        awesomeValidation.addValidation(this,R.id.edit_lname,"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$",R.string.lastname_error);
        awesomeValidation.addValidation(this,R.id.edit_phonenumber,"(?:\\+977[- ])?\\d{2}-?\\d{7,8}",R.string.phone_error);
        awesomeValidation.addValidation(this,R.id.edit_password,".{4,8}",R.string.password_error);
        awesomeValidation.addValidation(this,R.id.edit_confpassword,R.id.edit_password,R.string.conformpass_error);
        awesomeValidation.addValidation(this,R.id.edit_address, RegexTemplate.NOT_EMPTY,R.string.address_error);
        appCompatButton.setOnClickListener(this);

    }
    private void submitForm(){
        //first validate the form then move ahead
        //if this becomes true that means validation is successfull
        if (awesomeValidation.validate()) {
            Toast.makeText(this, "Validation Successful", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this, "Validation Failed", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onClick(View view) {
        if(view==appCompatButton){
            submitForm();
        }
    }
    }