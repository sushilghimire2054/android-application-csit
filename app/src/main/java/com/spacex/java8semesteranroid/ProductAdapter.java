package com.spacex.java8semesteranroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductVH> {
    private List<Product> productList=new ArrayList<>();
    private Context context;


    ProductAdapter(List<Product> productList, Context mContext){
        this.context=mContext;
        this.productList.addAll(productList);
    }
    @NonNull
    @Override
    public ProductVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
//        product_layout_recycle is a single row
        View viewData=layoutInflater.inflate(R.layout.product_layout_recycle,null);
        return new ProductVH(viewData);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductVH holder, int position) {
        holder.txtTitile.setText(productList.get(position).getTitle());
        holder.txtDescription.setText(productList.get(position).getDescription());
        holder.txtUnit.setText(productList.get(position).getUnit());
        holder.txtPrice.setText(productList.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
