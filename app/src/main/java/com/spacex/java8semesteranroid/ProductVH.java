package com.spacex.java8semesteranroid;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProductVH extends RecyclerView.ViewHolder {
    public TextView txtTitile,txtDescription,txtUnit,txtPrice;
    public ProductVH(@NonNull View itemView) {
        super(itemView);
        txtTitile=itemView.findViewById(R.id.txtProductTitle);
        txtDescription=itemView.findViewById(R.id.txtDescription);
        txtUnit=itemView.findViewById(R.id.txtProductUnit);
        txtPrice=itemView.findViewById(R.id.txtPrice);
    }
}
