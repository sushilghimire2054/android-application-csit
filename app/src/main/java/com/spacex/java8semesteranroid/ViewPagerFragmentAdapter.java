package com.spacex.java8semesteranroid;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewPagerFragmentAdapter extends FragmentStateAdapter {
    private String[] titles=new String[]{"Login","Register"};
    public ViewPagerFragmentAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
       switch (position){
           case 0:
               return new HomeFragment();
           case 1:
               return new SignupFragment();
       }
       return new HomeFragment();
    }

    @Override
    public int getItemCount() {
        return titles.length;
    }
}
