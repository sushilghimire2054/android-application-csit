package com.spacex.java8semesteranroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class ProductListActivity extends AppCompatActivity {
    private List<Product> productList;
    private ProductAdapter productAdapter;
    private RecyclerView recyclerView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        initToolBar();
        findViewsById();
        setListView();

    }
    private void initToolBar(){
        toolbar=findViewById(R.id.toolbar_id_textview);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
//    RecyclerView.Adapter - To handle the data collection and bind it to the view
//    LayoutManager - Helps in positioning the items
//    ItemAnimator - Helps with animating the items for common operations such as Addition or Removal of item
    private void findViewsById(){
        recyclerView=findViewById(R.id.recycle_view);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.setLayoutManager(gridLayoutManager);
    }
    private void setListView() {
//        creating the Product objects
        Product productOne=new Product("Freeze","3","Nice one want to buy","12000");
        Product productTwo=new Product("Refrigerator","4","Nice one want to buy","18000");
        Product productThree=new Product("Fan","10","Nice one want to buy","12000.00");
        Product productFour=new Product("Mixture","5","Nice one want to buy","12000.00");
        Product productFive=new Product("Grinder","3","Nice one want to buy","12000.00");

//        add the objects into the arrayList
        productList=new ArrayList<>();
        productList.add(productOne);
        productList.add(productTwo);
        productList.add(productThree);
        productList.add(productFour);
        productList.add(productFive);
        productAdapter=new ProductAdapter(productList, this);
        recyclerView.setAdapter(productAdapter);
    }
}