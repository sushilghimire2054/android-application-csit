package com.spacex.java8semesteranroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class DashBoardSignActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private String editUsername;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_sign);
        initToolBar();
        editUsername=getIntent().getStringExtra("username");
        textView=findViewById(R.id.user_dashboard_sign_id);
        textView.setText("Hello"+"  "+editUsername);
        Toast.makeText(DashBoardSignActivity.this,"welcome to dashboard activity",Toast.LENGTH_SHORT).show();
    }
    public void initToolBar(){
        toolbar=findViewById(R.id.toolbar_dash_id);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            Intent intent=new Intent(DashBoardSignActivity.this,MainActivity.class);
            startActivity(intent);
            Toast toast=Toast.makeText(DashBoardSignActivity.this,R.string.toast_message,Toast.LENGTH_SHORT);
            View view=toast.getView();
            view.setBackgroundResource(R.drawable.shape_button_back_to);
            toast.show();
        }
        return super.onOptionsItemSelected(item);
    }
}