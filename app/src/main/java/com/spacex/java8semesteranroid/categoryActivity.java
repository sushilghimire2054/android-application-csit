package com.spacex.java8semesteranroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;


public class categoryActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        initToolBar();
        getSupportFragmentManager().beginTransaction().add(R.id.frmContainer,new DashboardFragment()).commit();
    }
    public void initToolBar(){
        toolbar=findViewById(R.id.toolbarid);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Vedas College");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//show the back button
        getSupportActionBar().setDisplayShowTitleEnabled(true);//show toolbar title
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            getSupportFragmentManager().beginTransaction().replace(R.id.frmContainer,new HomeFragment()).commit();
        }
        return super.onOptionsItemSelected(item);
    }
}