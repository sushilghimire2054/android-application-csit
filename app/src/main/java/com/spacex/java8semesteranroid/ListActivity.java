package com.spacex.java8semesteranroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    private ListView listView;
    private Toolbar toolbar;
    private ListAdapter listAdapter;
    private List<Product> productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        checkConnection();
        initToolBar();
        findViewsById();
        setListView();
    }
    private void initToolBar(){
        toolbar=findViewById(R.id.toolbar_id_textview);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    private void findViewsById(){
        listView=findViewById(R.id.list_view);
    }
    private void setListView() {
//        creating the Product objects
        Product productOne=new Product("Freeze","3","Nice one want to buy","12000");
        Product productTwo=new Product("Refrigerator","4","Nice one want to buy","18000");
        Product productThree=new Product("Fan","10","Nice one want to buy","12000.00");
        Product productFour=new Product("Mixture","5","Nice one want to buy","12000.00");
        Product productFive=new Product("Grinder","3","Nice one want to buy","12000.00");

//        add the objects into the arrayList
        productList=new ArrayList<>();
        productList.add(productOne);
        productList.add(productTwo);
        productList.add(productThree);
        productList.add(productFour);
        productList.add(productFive);
        listAdapter=new ListAdapter(productList, this);
        listView.setAdapter(listAdapter);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            Toast.makeText(this, "Back button clicked", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
    public void checkConnection(){
        ConnectivityManager manager=(ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=manager.getActiveNetworkInfo();
        if(networkInfo!=null){
            if(networkInfo.getType()==ConnectivityManager.TYPE_WIFI){
                Toast.makeText(this, "Wifi enabled", Toast.LENGTH_SHORT).show();
            }
            if(networkInfo.getType()==ConnectivityManager.TYPE_MOBILE){
                Toast.makeText(this, "Data network enabled", Toast.LENGTH_SHORT).show();
            }

        }
        else{
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();

        }
    }
}