package com.spacex.java8semesteranroid;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

public class HomeFragment extends Fragment implements View.OnClickListener{
    private EditText editUserName,editUserPassword;
    private AppCompatButton btnLogin;
    private View mainView;
    private AwesomeValidation awesomeValidation;
    public HomeFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        inflation converting XML appearance definition into views objects in code.
        mainView=inflater.inflate(R.layout.fragment_home, container, false);
        editUserName=mainView.findViewById(R.id.edit_username);
        editUserPassword=mainView.findViewById(R.id.edit_password);
        btnLogin=mainView.findViewById(R.id.button_login);
//        assign
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
//        add awesome validation
        awesomeValidation.addValidation(editUserName,"^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$", "Please enter valid username");
        awesomeValidation.addValidation(editUserPassword,"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$", "Please enter valid password");
        btnLogin.setOnClickListener(this);
//        findByViews();
        return mainView;
    }
    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.button_login){
            String username=editUserName.getText().toString();
            String userPassword=editUserPassword.getText().toString();
            if(awesomeValidation.validate()) {
                Intent intent = new Intent(getActivity(), DashBoardSignActivity.class);
                intent.putExtra("username",username);
                intent.putExtra("password",userPassword);
                Toast.makeText(getActivity(),"validate successful",Toast.LENGTH_SHORT).show();
                startActivity(intent);

            }
            else
            {
                Toast.makeText(getActivity(),"validation failed",Toast.LENGTH_SHORT).show();
            }

        }
    }
}