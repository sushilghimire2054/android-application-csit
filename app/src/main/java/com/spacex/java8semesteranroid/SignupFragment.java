package com.spacex.java8semesteranroid;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class SignupFragment extends Fragment implements View.OnClickListener{

    private EditText editFname,editLname,editPhoneNumber,editPassword,editConfPassword,editAddress;
    private AppCompatButton signupBtn;
    private View signView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        signView=inflater.inflate(R.layout.signup_layout, container, false);
        findByViews();
        return signView;
    }
    private void findByViews(){
        editFname=signView.findViewById(R.id.edit_lname);
        editLname=signView.findViewById(R.id.edit_lname);
        editPhoneNumber=signView.findViewById(R.id.edit_phonenumber);
        editPassword=signView.findViewById(R.id.edit_password);
        editConfPassword=signView.findViewById(R.id.edit_confpassword);
        editAddress=signView.findViewById(R.id.edit_address);
        signupBtn=signView.findViewById(R.id.signup_btn);
        signupBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.signup_btn){
            String editFirstName=editFname.getText().toString();
            String editLastName=editLname.getText().toString();
            if(!editFirstName.isEmpty()&& !editLastName.isEmpty()){
                Intent intent= new Intent(getActivity(),MainActivity.class);
                startActivity(intent);
                Toast.makeText(getActivity(),"Register Successfully",Toast.LENGTH_SHORT).show();

            }
            else{
                Toast.makeText(getActivity(),"All fields required ",Toast.LENGTH_SHORT).show();

            }


        }

    }
}