package com.spacex.java8semesteranroid;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

public class DashboardFragment extends Fragment {
    private View mainView;
    private CheckBox checkJava;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment return the view
        mainView= inflater.inflate(R.layout.fragment_dashboard, container, false);
        findViews();
        return mainView;
    }
    private void findViews(){
        checkJava=mainView.findViewById(R.id.chkJava);
    }
}