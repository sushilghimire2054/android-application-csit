package com.spacex.java8semesteranroid;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private EditText editUsername,editPassword;
    private AppCompatButton loginBtn;
    private TextView clickShow;
    private CheckBox chkPython,chkJava,chkCsharp,chkReactjs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_form_layout);
        initToolBar();
        findViews();
    }
    private void findViews(){
        editUsername=findViewById(R.id.edit_username);
        editPassword=findViewById(R.id.edit_password);
        clickShow=findViewById(R.id.text_click);
        chkPython=findViewById(R.id.chkPython);
        chkJava=findViewById(R.id.chkJava);
        chkCsharp=findViewById(R.id.chkCsharp);
        chkReactjs=findViewById(R.id.chkReactjs);
        loginBtn=findViewById(R.id.button_login);
       chkPython.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String chkPyth=chkPython.getText().toString();
                Log.d("checked",chkPyth);
           }
       });
//        next way to register source btn
        loginBtn.setOnClickListener(this);
    }
    public void initToolBar(){
        toolbar=findViewById(R.id.toolbarid);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Vedas College");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//show the back button
        getSupportActionBar().setDisplayShowTitleEnabled(true);//show toolbar title
    }
    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.button_login){
            String username=editUsername.getText().toString();
            String password=editPassword.getText().toString();
            String language="";
            if(chkJava.isChecked()){
                language=language+""+chkJava.getText().toString();
            }
            if(chkPython.isChecked()){
                language=language+""+chkPython.getText().toString();
            }
            if(chkCsharp.isChecked()){
                language=language+""+chkCsharp.getText().toString();
            }
            if(chkReactjs.isChecked()){
                language=language+""+chkReactjs.getText().toString();
            }
            Log.d("known langauge",language);
            Log.d("username",username+" "+password);
            clickShow.setText("Username"+username+ "password"+password);
            if(!username.isEmpty() && !password.isEmpty()){
//                navigate one activity to activity also known as communication between android activity
                Intent intent=new Intent(LoginActivity.this,DashboardTab.class);
//                send data from one activity to another activity
                intent.putExtra("Username",username);
                intent.putExtra("data",10);
                startActivity(intent);
//                startActivity create a new activity in this component DashboardTab is the startActivity
            }
            else{
                Toast.makeText(LoginActivity.this,"Username and password cannot be empty",Toast.LENGTH_SHORT).show();
            }
        }
//        startActivity for resolve-remember to learn
    }
}
