package com.spacex.java8semesteranroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.material.tabs.TabLayout;

public class DashboardTab extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private String username,password;
    private int data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboradtab);
        username=getIntent().getStringExtra("username");
        password=getIntent().getStringExtra("password");
        data=getIntent().getIntExtra("data",0);
        Toast.makeText(DashboardTab.this,username+" "+password,Toast.LENGTH_SHORT).show();
        Toast.makeText(DashboardTab.this,"Login Successfully",Toast.LENGTH_LONG).show();

        initToolBar();
        findViews();
        setupViewPager();
        tabLayout.setupWithViewPager(viewPager);//sync with view pager and tablayout
//        getSupportFragmentManager().beginTransaction().add(R.id.frmContainer,new DashboardFragment()).commit();
    }
    private void findViews(){
        tabLayout=findViewById(R.id.tablayout);
        viewPager=findViewById(R.id.viewpager);
    }
    public void initToolBar(){
        toolbar=findViewById(R.id.toolbarid);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Vedas College");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//show the back button
        getSupportActionBar().setDisplayShowTitleEnabled(true);//show toolbar title
    }
    private void setupViewPager(){
        adapter= new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(),"Home");
        adapter.addFragment(new DashboardFragment(),"Dashboard");
        viewPager.setAdapter(adapter);

        //fragment manager maintain the stack fragment
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            getSupportFragmentManager().beginTransaction().replace(R.id.frmContainer,new DashboardFragment()).commit();
        }
        return super.onOptionsItemSelected(item);
    }
}