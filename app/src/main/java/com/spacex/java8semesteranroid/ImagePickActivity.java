package com.spacex.java8semesteranroid;

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ImagePickActivity extends AppCompatActivity {
    private ImageView imageUpload;
    private Button btnUpload;
    private Dialog originalDialog;
    private AlertDialog.Builder alertDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_pick);
        findViews();
    }
    private void findViews() {
        imageUpload = findViewById(R.id.image_upload);
        btnUpload = findViewById(R.id.btn_upload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
    }

    public void showDialog() {
        alertDialogBuilder = new AlertDialog.Builder(this);
        View viewDialog = View.inflate(this, R.layout.dialog_upload_photo, null);
        TextView txtUpload = viewDialog.findViewById(R.id.txt_take_photo);
        TextView txtChooseFromGallary = viewDialog.findViewById(R.id.txt_upload_from_gallary);
        TextView txtCancel = viewDialog.findViewById(R.id.txt_cancel);
        alertDialogBuilder.setView(viewDialog);
        originalDialog = alertDialogBuilder.create();

        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                originalDialog.dismiss();

            }
        });
        txtChooseFromGallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                originalDialog.dismiss();
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                originalDialog.dismiss();
            }
        });
        originalDialog.getWindow().setDimAmount(0.7f);
        originalDialog.setCanceledOnTouchOutside(false);
        originalDialog.show();
    }
}