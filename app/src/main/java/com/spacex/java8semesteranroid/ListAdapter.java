package com.spacex.java8semesteranroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends BaseAdapter {
    private List<Product> productList=new ArrayList<>();
    private Context context;


    ListAdapter(List<Product> productList, Context mContext){
        this.context=mContext;
        this.productList.addAll(productList);
    }

    @Override
    public int getCount() {
//        how many times view must me count
        return productList.size();
    }

    @Override
    public Object getItem(int i) {
//        return object with particular position
        return productList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View viewData=layoutInflater.inflate(R.layout.adapter_product,null);
        ViewHolder holder=new ViewHolder(viewData);
        holder.txtTitile.setText(productList.get(position).getTitle());
        holder.txtDescription.setText(productList.get(position).getDescription());
        holder.txtUnit.setText(productList.get(position).getUnit());
        holder.txtPrice.setText(productList.get(position).getPrice());

//        setUpData(view,position);
//        every time make the view with position
        return viewData;
    }
    public static class ViewHolder{
        //        particular item in list view is called viewholder
        public TextView txtTitile,txtDescription,txtUnit,txtPrice;
        public ViewHolder(View view){
            txtTitile=view.findViewById(R.id.txtProductTitle);
            txtDescription=view.findViewById(R.id.txtDescription);
            txtUnit=view.findViewById(R.id.txtProductUnit);
            txtPrice=view.findViewById(R.id.txtPrice);
        }
    }

//    private void setUpData(View view,int position) {
//        LinearLayout linearLayout = view.findViewById(R.id.linear_layout_id);
//        TextView textViewTitle = view.findViewById(R.id.txtProductTitle);
//        textViewTitle.setText("Title" + ": " + productList.get(position).getTitle());
//        TextView textViewUnit = view.findViewById(R.id.txtProductUnit);
//        textViewUnit.setText("Unit" + ": " + productList.get(position).getUnit());
//        TextView textViewDesc = view.findViewById(R.id.txtDescription);
//        textViewDesc.setText("Description" + ": " + productList.get(position).getDescription());
//        TextView textViewPrice = view.findViewById(R.id.txtPrice);
//        textViewPrice.setText("Price" + ": " + productList.get(position).getPrice());
//        linearLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(context, "Position" + position, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//    }
}
